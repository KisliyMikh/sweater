delete from message;

insert into message(id, text, tag, user_id) values
(1, 'first', 'my-tag', 2),
(2, 'second', 'pes', 2),
(3, 'third', 'my-tag', 2),
(4, 'fourth', 'molly', 2);

alter sequence hibernate_sequence restart with 10;