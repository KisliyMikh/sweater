delete from user_role;
delete from usr;

insert into usr(id, active, password, username)
values (2, true ,'$2a$08$tfXV5/Wm7va7SQADJeoRBeDmtc/WkjizaWW9./WlQFF3F4U.F6N9W','admin');

insert into user_role(user_id, roles)
values (2, 'USER'), (2, 'ADMIN');
