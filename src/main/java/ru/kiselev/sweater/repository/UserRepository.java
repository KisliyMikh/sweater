package ru.kiselev.sweater.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kiselev.sweater.models.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findByActivationCode(String code);
}
