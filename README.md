# Sweater  

## Используемый стек

* Java SE 8
* Maven 3.6
* Spring Boot
* Hibernate
* Spring Security
* Freemarker
* Lombok  

## Обратная свзяь
mail : *[kmsperm97@mail.ru]()* 

## Сбор и запуск приложения  

### Команды для сборки приложения  

* mvn [clean]()  - *очистить проект*
* mvn [install]() -  *установить проект*

### Команды для запуска приложения  

* java -jar sweater-0.0.1-SNAPSHOT.jar
